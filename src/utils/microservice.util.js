const fs = require('fs'),
_PROMPT = require('./prompt.util'),
_FILES = require ('./files.util'),
_PRINT = require('./print.util'),
path = require('path');

const SKIP = ['node_modules', 'dist', '.nyc_output', 'coverage'];
const MAPPING = {
    name: {
        find: 'MICRO_SERVICE_NAME',
        replace: '',
    },
}
function copyAllFiles(originPath, destinationPath, name, arrayOfFiles){
    files = fs.readdirSync(originPath);
    arrayOfFiles = arrayOfFiles || [];
    files.forEach(async function(file) {
        try {
            const fileOriginPath = path.join(originPath, '/', file)
            const fileDestinationPath = path.join(destinationPath, '/', file)
            if (fs.statSync(fileOriginPath).isDirectory() && !SKIP.includes(file)) {
                _FILES.createDirectory(fileDestinationPath);
                arrayOfFiles = copyAllFiles(fileOriginPath, destinationPath+'/'+file, name, arrayOfFiles);
                // CREATE DIRECTORY
                // PASS NEW PATH WITH ALL FILES TO NEW DES PATH
            } else if(!fs.statSync(fileOriginPath).isDirectory() && !SKIP.includes(file)) {
                // COPY FILE
                const data = await fs.readFileSync(fileOriginPath).toString();
                const findRegex = new RegExp(MAPPING.name.find, 'gi');
                const newData = data.replace(findRegex, name)
                fs.writeFileSync(fileDestinationPath, newData);
                arrayOfFiles.push(fileOriginPath);
            }            
        } catch (error) {
            throw(Error(error.message));
        }
    })
    return arrayOfFiles;
}

function createMicroService (targetDirectory) {
    return new Promise(async (resolve, reject) => {
        try {
            // MICRO SERVICE ORIGIN PATH
            const PATH = path.join(__dirname, '../lib/components/micro-service/1.0.0/');
            
            // PROMPT NEW MICRO-SERVICE QUESTIONER 
            const { name, port } = await _PROMPT.promptCreateMicroService();
            MAPPING.name.replace = name;

            // NEW FINAL DESTINATION PATH
            const DES_PATH = targetDirectory+'/'+name;

            // DEST IF SERVICE NAME ALREADY EXIST IN FINAL DESTINATION
            const dirExist = _FILES.fileExist(DES_PATH);
            if(dirExist){ reject('Service already exist') }

            // CREATE FINAL DIRECTORY
            _FILES.createDirectory(DES_PATH);

            // COPY NEW MICRO-SERVICE FILES TO FINAL DIRECTORY
            // await fs.writeFileSync(`${DES_PATH}/FILES.json`, JSON.stringify(files));
            copyAllFiles(PATH, DES_PATH, name);
            
            // INFO
            _PRINT.print(`SUCCESSFULLY CREATED ${name} MICRO SERVICE, HAPPY HACKING!`, 'green');
            _PRINT.print('To get started run scripts, npm [install, run build, start]\n\n', 'yellow');
            
            resolve();
        } catch (error) {
            reject(error.message);
        }
    });
};

module.exports = {
    SKIP,
    MAPPING,
    createMicroService
}
