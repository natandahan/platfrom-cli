const _ = require('./index'),
inquirer = require('inquirer');

module.exports.MENU_CHOICES = {
    START_APP: '1. Start App',
    CREATE_MICRO_SERVICE: '2. Create micro service',
    CREATE_MICRO_FRONTEND: '3. Create micro frontend',
    EXIT: '4. Exit'
}

module.exports.promptMenu = () => {
    return new Promise((resolve, reject) => {
        const select = [
            { 
                name: 'selected',
                type: 'list',
                message: 'What would you like to do',
                choices: [
                    this.MENU_CHOICES.START_APP,
                    this.MENU_CHOICES.CREATE_MICRO_SERVICE,
                    this.MENU_CHOICES.CREATE_MICRO_FRONTEND,
                    this.MENU_CHOICES.EXIT
                ],
                validate: (val) => {
                    if(val.length){ return true; }
                    return 'Please select';
                }
            }
        ];
        resolve(inquirer.prompt(select));
    })
};

module.exports.promptCreateMicroService = () => {
    return new Promise((resolve, reject) => {
        const questions = [
            {
                name: 'name',
                type: 'input',
                message: 'Name of the micro-service',
                validate: (val) => {
                    if(val.length && !val.includes(' ')){ return true; }
                    return 'Value can`t be empty, no spaces allowed, use _ instead';
                }
            },
            {
                name: 'port',
                type: 'input',
                message: 'Port of the micro-service',
                validate: (val) => {
                    if(val.length && !val.includes(' ')){ return true; }
                    return 'Value can`t be empty, no spaces allowed, use _ instead';
                }
            }
        ];
        resolve(inquirer.prompt(questions));
    })
};
