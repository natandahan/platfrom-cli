const print = require('./print.util'),
obj = require('./object.util'),
files = require('./files.util'),
prompt = require('./prompt.util'),
ms = require('./microservice.util');

module.exports = {
    ...ms,
    ...obj,
    ...files,
    ...print,
    ...prompt
};
