const fs = require('fs'),
path = require('path');

/**
 * @description create directory for given path
 * @param {string} path to directory
 * @returns {void} void
 */
function createDirectory(path) {
    return fs.mkdirSync(path);
}

/**
 * @description get current directory, relative or absolute 
 * @param {*} { relative = false }
 * @returns {string} current directory path 
 */
function currentDirectory({ relative = false }){
    return relative ? path.basename(process.cwd()) : process.cwd()
};

/**
 * @description checks if file exists at given path
 * @param {string} path
 * @returns {boolean} true | false
 */
function fileExist(path) {
    return fs.existsSync(path);
};

/**
 * @description list all files in directory and sub-directories
 * @param {string} dirPath directory path 
 * @param {string[]} arrayOfFiles use as container for all files
 * @returns { string[] } array of paths, all files in directory
 */
function getAllFiles(dirPath, arrayOfFiles){
    const files = fs.readdirSync(dirPath);
    arrayOfFiles = arrayOfFiles || [];
    files.forEach(function(file) {
        if (fs.statSync(dirPath + '/' + file).isDirectory() && !SKIP.includes(file)) {
            arrayOfFiles = getAllFiles(dirPath + '/' + file, arrayOfFiles);
        } else {
            arrayOfFiles.push(path.join(dirPath, '/', file));
        }
    })
    return arrayOfFiles;
}

/**
 * @description check if file in given path is a directory 
 * @param {string} path 
 * @returns {boolean} true | false
 */
function isDirectory (path) {
    return fs.statSync(path).isDirectory();
}

module.exports = {
    createDirectory,
    currentDirectory,
    fileExist,
    getAllFiles,
    isDirectory
}
