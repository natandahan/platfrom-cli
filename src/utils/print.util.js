const chalk = require('chalk'),
clear = require('clear'),
figlet = require('figlet');

const _print = { bold: false, layout: 'full' };
module.exports.print = (message, color='green', options) => {
    const { bold, layout } = options ? Object.assign({}, _print, options) : _print;
    const text = typeof message === 'object' ? JSON.stringify(message) : message;
    bold ? console.log(chalk[color](figlet.textSync(text, { horizontalLayout: layout }))) : console.log(chalk[color](text))
    return;
};