#!/usr/bin/env node

const clui = require('clui'),
chalk = require('chalk'),
clear = require('clear'),
_ = require('./utils'),
figlet = require('figlet'),
pkg = require('../package'),
configstore = require('configstore');

const spinner = new clui.Spinner();
const osStore = new configstore(pkg.name);
const settings = osStore.all;

(async () => {
    try {
        let userMenuSelect;
        const { EXIT, CREATE_MICRO_FRONTEND, CREATE_MICRO_SERVICE, START_APP } = _.MENU_CHOICES
        
        /* INIT APP */
        _.print('Platform CTL', 'yellow', { bold: true });
        if(_.isObjEmpty(settings)){ 
            _.print('NO SETTINGS FOUND ON OS') // TODO INIT  
        };

        /* MAIN MENU */
        userMenuSelect = await _.promptMenu();
        while(userMenuSelect.selected !== EXIT){
            switch(userMenuSelect.selected){
                case START_APP: { _.print('now starting app'); break; }
                case CREATE_MICRO_SERVICE: { 
                    await _.createMicroService(_.currentDirectory({}));
                    break; 
                }
                case CREATE_MICRO_FRONTEND: { _.print('creating micro frontend');  break;}
                default: { _.print('invalid choice', 'red'); break; }
            }
            userMenuSelect = await _.promptMenu();
        };
        _.print(userMenuSelect);
        
    } catch (error) {
        console.log(chalk.red(error));
    }
})()

