import app, { securityLogger } from './app'
import CONFIG from '@env'

app.listen(CONFIG.PORT, () => {
    securityLogger.warn(`server started at http://localhost:${ CONFIG.PORT } --${CONFIG.ENV} --pid-${process.pid} --auth=${CONFIG.AUTH}`)
})
