
import { Document, model, Schema } from 'mongoose'

export enum STATE {
    ACTIVE = 'ACTIVE',
    CREATED = 'CREATED',
    DELETED = 'DELETED',
    DISABLED = 'DISABLED',
    SUSPENDED = 'SUSPENDED',
    UNDER_REVIEW = 'UNDER_REVIEW',
    PENDING_VALIDATION = 'PENDING_VALIDATION',
}

export interface IUser extends Document {
    email: string
    phone?: string
    title?: string
    location?: any
    gender?: string
    lastName: string
    username: string
    firstName: string
}

export const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    gender: {
        type: String,
    },
    location: {
        type: Object
    },
    phone : {
        type: String
    },
    picture: {
        type: String,
    },
    title: {
        type: String,
    },
    status: {
        type: String,
        default: STATE.CREATED
    }
}, { timestamps: true }) as any

userSchema.index​​({
    firstName: 'text',
    lastName: 'text',
    email: 'text',
    location: 'text',
    phone: 'text'
})

export default model<IUser>('User', userSchema) as any
