
import { Document, model, Schema } from 'mongoose'

export interface IComment extends Document {
    author: string
    text: string
}

const commentSchema = new Schema({
    author: {
        type: String,
    },
    text: {
        type: String,
    },
}, { timestamps: true })

export default model<IComment>('Comment', commentSchema)
