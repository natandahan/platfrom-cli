import IJob from '../job'

const cpuMonitor = {
    job: 'cpu_check',
    every: '20 seconds',
    title: 'Cpu Checker',
    description: 'test process cpu usage every',
    data: null,
    exec: ({}, done: any) => {
        console.log(`PROCESS PID ${process.pid} CPU USAGE ${JSON.stringify(process.cpuUsage())}`)
        done()
    }
} as IJob

export default cpuMonitor
