declare interface IJob {
    data?: any
    job: string
    title: string
    every?: string
    schedule?: string
    description?: string
    exec: (job?: any, done?: any) => void | Promise<any>
    fail?: (job?: any, done?: any) => void | Promise<any>
    start?: (job?: any, done?: any) => void | Promise<any>
    success?: (job?: any, done?: any) => void | Promise<any>
    complete?: (job?: any, done?: any) => void | Promise<any>
}

export default IJob
