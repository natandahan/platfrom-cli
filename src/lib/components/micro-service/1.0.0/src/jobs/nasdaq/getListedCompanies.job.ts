import IJob from '../job'
import { fetch, csv } from '@app'

const nasdaqCompanies = {
    job: 'get_all_companies_in_nasdaq',
    every: '2 minutes',
    title: 'NASDAQ Companies',
    desc: 'get csv list of all companies listed in nasdaq',
    exec: async ({}, done: any) => {
        try {
            const fetchCsvRespond = await fetch(process.env.NASDAQ_CSV)
            const csvFile = await fetchCsvRespond.text()
            const jsonArray = await csv().fromString(csvFile)
            console.log(jsonArray)
            done()
        } catch (err) {
            throw new Error(err)
        }
    },
} as IJob

export default nasdaqCompanies
