import IJob from './job'
import agenda from 'agenda'
import * as jobs from './jobs'
import CONFIG, { mongoUri } from '@env'
import { securityLogger, accessLogger } from '@app'

const JOBS = jobs as any
const jobTypes = [] as string []
const jobKeys = Object.keys(JOBS)
const scheduler = new agenda({
    ...CONFIG.SCHEDULER,
    db: {
        address: mongoUri(),
        collection: 'SCHEDULER_JOBS'
    },
})

/**
 * @defined
 * 1. Iterate through pre-defined jobs, validate job name is not taken => defined
 */
jobKeys.forEach(async (j) => {
    try {
        if (!jobTypes.includes(JOBS[j].job)) {
            jobTypes.push(JOBS[j].job)
            const {job, exec, start, success, fail, complete} = JOBS[j] as IJob
            await scheduler.define(job, exec)
            if (fail) { await scheduler.define(`fail:${job}`, fail) }
            if (start) { await scheduler.define(`start:${job}`, start) }
            if (success) { await scheduler.define(`success:${job}`, success) }
            if (complete) { await scheduler.define(`complete:${job}`, complete) }
        } else {
            accessLogger.info(`Oh oh job name [${JOBS[j].job}] is already registered in job types`)
        }
    } catch (err) {
        accessLogger.error(err)
    }
})

/**
 * @onReady
 * 1. Start agenda
 * 2. Iterate through pre-defined jobs, if defined and not register yet => register
 */
scheduler.on('ready', async () => {
    try {
        await scheduler.start()
        securityLogger.warn(`starting scheduler worker`)
        jobKeys.forEach(async (j) => {
            const isRegister = await scheduler.jobs({name: JOBS[j].job }) || undefined
            if (jobTypes.includes(JOBS[j].job) && isRegister.length === 0) {
                const {job, data, every} = JOBS[j] as IJob
                const jobOptions = data ? data : undefined
                const createdJob = scheduler.create(job, { ...jobOptions })
                await createdJob.repeatEvery(every).save()
            } else {
                isRegister.length > 0 ?
                    accessLogger.warn(`${j} already exist in db`)
                    :
                    accessLogger.warn(`${j} is not defined`)
            }
        })
    } catch (err) {
        accessLogger.error(err)
    }
})

export { jobTypes }
export default scheduler
