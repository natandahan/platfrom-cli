import { DB } from '@app'
import { bool } from 'aws-sdk/clients/signer'

// TODO REMOVE ALL OPTIONALS ? ON REQUIRED FILED

export interface IENV {
    PORT?: number
    ENV?: string
    TIMEOUT?: string
    NODE_ENV?: string
    AUTH?: boolean
    NAME?: string
    DESCRIPTION?: string
    FEATURES?: string
    AUTHOR?: string
    DB?: {
        port?: number
        host?: string
        username?: string
        password?: string
        name?: string
        options?: object
        defaults?: {
            limit?: number
        }
    }
    OIDC: {
        keys?: string
        apiKey?: string
        issuer?: string
        region?: string
        poolId?: string
        endpoint?: string
        clientId?: string
        cookieId?: string
        redirectUri?: string
    },
    AWS: {
        accessKeyId?: string
        secretAccessKey?: string
    },
    HELMET: {
        hsts?: boolean
        xssFilter?: boolean
        frameguard?: boolean
    },
    SCHEDULER: {
        lockLimit: number
        processEvery: string
        maxConcurrency: number
        defaultLockLimit: number
        defaultConcurrency: number
        defaultLockLifetime: number
    },
    LOGGER: {
        level: string
        label: string
        format: string
        timestamp: string
    }
}

const ENV = {
    AUTH: process.env.AUTH ? (process.env.AUTH === 'true') : true,
    PORT: process.env.PORT || 8080,
    TIMEOUT: process.env.TIMEOUT || '3s',
    ENV: process.env.ENV || 'development',
    NODE_ENV: process.env.NODE_ENV || 'development',
    NAME: process.env.SERVICE_NAME || 'Users Server',
    DESCRIPTION: process.env.SERVICE_DESC || 'User management service',
    FEATURES: process.env.FEATURES || '[{ "DB": "MongoDB"}, {"AUTH": "Amazon Cognito"}, {"LOGGING": "Winston Logging"}, {"JOB_SCHEDULER": "Agenda"}, {"DOCUMENTATION": "Swagger UI"}]',
    DB: {
        tag: 'MongoDB',
        port: 27017 || process.env.DB_PORT,
        host: 'localhost' || process.env.DB_IP,
        username: process.env.DB_USER || 'CLT_007',
        password: process.env.DB_PASS || 'CLT_007',
        name: process.env.DB_NAME || 'TEST_DB_007',
        options : {
            useCreateIndex: true,
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        },
        defaults : {
            limit: 100,
        }
    },
    OIDC: {
        tag: 'AWS Cognito',
        cookieId: 'ndc',
        region: 'us-west-2',
        poolId: 'us-west-2_LmrSqblMs',
        redirectUri: 'http://localhost:8080/api/v1/oidc',
        clientId: '5ongk44409sqjek7qlul3vtl12',
        apiKey: '1l9jvgb4573m2lvm4cgod8hleofdh4sr2d4s1d0g6p5s8dhueph7',
        endpoint: 'https://natandahan.auth.us-west-2.amazoncognito.com',
        issuer: 'https://cognito-idp.us-west-2.amazonaws.com/us-west-2_LmrSqblMs',
        keys: 'https://cognito-idp.us-west-2.amazonaws.com/us-west-2_LmrSqblMs/.well-known/jwks.json',
    },
    AWS: {
        accessKeyId: 'AKIASQO7GBTRLDFECWWH',
        secretAccessKey: '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'
    },
    HELMET : {
        hsts: process.env.ENV  === 'production' ? true : false,
        xssFilter: process.env.ENV  === 'production' ? true : false,
        frameguard: process.env.ENV  === 'production' ? true : false
    },
    SCHEDULER: {
        tag: 'Agenda',
        lockLimit: 0,
        maxConcurrency: 20,
        defaultLockLimit: 0,
        defaultConcurrency: 5,
        processEvery: '30 seconds',
        defaultLockLifetime: 10000,
    },
    LOGGER: {
        tag: 'Winston',
        level: process.env.LOGGER_LEVEL || 'warn',
        format: process.env.LOGGER_FORMAT || 'combined',
        label: process.env.LOGGER_LABEL || 'default-label',
        timestamp: process.env.timestamp || 'MM-DD-YY HH:mm:ss'
    },
} as IENV

export const mongoUri = () => {
    return `mongodb://${ENV.DB.username}:${ENV.DB.password}@${ENV.DB.host}:${ENV.DB.port}/${ENV.DB.name}?authSource=admin`
}

export default ENV
