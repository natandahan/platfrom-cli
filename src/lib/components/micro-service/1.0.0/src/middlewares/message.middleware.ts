import * as app from '@app'
import express from 'express'

interface IMessageResponse {
    time?: string
    status?: number
    message: {
        title?: string
        description?: string
    }
}

const DivStyle = `"height:100vh; width:100vw;
    background-repeat: no-repeat;
    background-size: cover;
    text-align: center;
    line-height: 100vh;
    background-image: url('https://images.pexels.com/photos/949587/pexels-photo-949587.jpeg?cs=srgb&dl=yellow-bokeh-photo-949587.jpg&fm=jpg')"
`

const TitleStyle = `"
    color: #fff;
    font-size: 3.2vw
"`

const generateMessage = (message: any) => {
    return `
        <body style="margin: 0; height: 100vh; width: 100vw;     overflow: hidden;">
            <div style=${DivStyle}>
            ${ message && message.title ? `<h3 style=${TitleStyle}>${message.title} ${message && message.description ? `<small><em>${message.description}</em></small>` : ''}</h3>` : ''}
        </body>
    `
}

export const MessageRespond = (data: IMessageResponse, req: express.Request, res: express.Response, nxt: any): express.Response | any => {
    const { CODES } = app
    if (data && data.message && data.message.title) {
        return res.status(data.status || CODES.OK).send(generateMessage(data.message)).end()
    }
    nxt()
}
