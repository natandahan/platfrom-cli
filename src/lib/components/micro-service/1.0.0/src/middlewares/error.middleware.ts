import * as app from '@app'
import express from 'express'

interface IErrorResponse {
    time?: string
    status: number
    message?: string
}

export const Http404 = (req: express.Request, res: express.Response): any => {
    const { moment, CODES } = app
    const err = {
        status: CODES.NOT_FOUND,
        message: `${req.method} '${req.path}' not found`,
        time: moment(Date.now()).utcOffset('-07:00').format('MMMM Do YYYY, h:mm:ssa')
    } as IErrorResponse
    res.status(err.status).json({
        errors: err.message,
        time: err.time
    })
}

export const HttpException = (err: IErrorResponse, req: express.Request, res: express.Response, nxt: any): express.Response | any => {
    const { CODES, moment } = app
    // logger.error(error.stack || error)
    console.log(err)
    if (err.message || err.status) {
        res.status(err.status || CODES.BAD_REQUEST).json({
            errors: err.message || 'Oops! Something went wrong.',
            time: moment(Date.now()).utcOffset('-07:00').format('MMMM Do YYYY, h:mm:ssa'),
        })
    } else {
        nxt()
    }
}
