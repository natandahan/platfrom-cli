import * as app from '@app'
import express from 'express'
import { IRequest } from '@app-types'
import { validateJwt, getIdpCurrent } from '@utils/index'
import { bool } from 'aws-sdk/clients/signer'

/**
 * @desc check if request had passed timeout
 */
export const useTimeout = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
    if (req.timedout) {
        // TODO LOG TIMEOUT REQUESTS
        res.status(app.CODES.REQUEST_TIMEOUT)
    }
    nxt()
}
