import * as app from '@app'
import express from 'express'
import { IRequest } from '@app-types'
import { validateJwt, getIdpCurrent } from '@utils/index'
import { bool } from 'aws-sdk/clients/signer'

/**
 * @desc check for valid oidc cookie
 * @valid continue
 * @invalid redirect to auth flow
 */
let pendingUri = ''
export const useAuth = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
    // TODO REMOVE IN PROD, FOR DEV USE ONLY
    if (!app.CONFIG.AUTH) { return nxt() }
    const { endpoint, clientId, cookieId } = app.CONFIG.OIDC
    const cookie = req.cookies ? req.cookies[cookieId] : undefined
    const bearerToken = req.headers.authorization ? req.headers.authorization.split(' ')[1] : undefined
    const token = cookie || bearerToken
    const uri = req.protocol + '://' + req.get('host') + '/api/v1/oidc'
    const reqUrl = req.protocol + '://' + req.get('host') + req.originalUrl
    const initOidcAuthFlow = () => {
        const redirectUrl = `${endpoint}/login?client_id=${clientId}&response_type=code&scope=profile+email+phone+openid&redirect_uri=${uri}`
        return res.redirect(redirectUrl)
    }
    try {
        const currentUser = await getIdpCurrent(token) as any
        const idCookieValid = await validateJwt(token) as bool

            // 1.FALSE => INIT AUTH FLOW
        if (!idCookieValid) {
            pendingUri = reqUrl
            if (req.cookies && req.cookies[cookieId]) {
                res.clearCookie(cookieId)
            }
            req.user = undefined
            return initOidcAuthFlow()
        }

            // 2.TRUE => PENDING URL
        if (idCookieValid && pendingUri && pendingUri !== reqUrl) {
            const temp = pendingUri
            pendingUri = undefined
            req.user = currentUser
            console.log(`REQ URL ${reqUrl}`)
            console.log(`NOW REDIRECTING ${pendingUri}`)
            return res.redirect(temp)
        }

        // 3.TRUE => CONTINUE
        if (idCookieValid) {
            req.user = currentUser
            nxt()
        }
    } catch (err) {
        // TODO SECURITY LOG -> EVENT AUTH FAIL, WHY?
        console.log(err)
        initOidcAuthFlow()
    }
}
