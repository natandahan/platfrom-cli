import * as app from '@app'
import express from 'express'
import { validationResult } from 'express-validator'

interface IMessageResponse {
    time?: string
    status?: number
    message: {
        title?: string
        description?: string
    }
}

export const useValidator = async (req: express.Request, _: express.Response, nxt: any): Promise<any> => {
    try {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            return nxt()
        } else {
            return nxt({ status: app.CODES.BAD_REQUEST, message: errors.array()})
        }
    } catch (error) {
        return nxt({ status: app.CODES.BAD_REQUEST, message: error.message})
    }
}
