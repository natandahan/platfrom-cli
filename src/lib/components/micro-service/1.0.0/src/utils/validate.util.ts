
import { CODES } from '@app'
import express from 'express'
import { validationResult } from 'express-validator'

export const httpValidate = (req: express.Request): Promise<any> => {
    return new Promise((resolve, reject) => {
        const errors = validationResult(req)
        if (errors.isEmpty()) {
            resolve({})
        } else {
            reject({ status: CODES.BAD_REQUEST, message: errors.array()})
        }
    })
}
