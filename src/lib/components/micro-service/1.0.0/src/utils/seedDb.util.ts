import fetch from 'node-fetch'
import User from '@models/user.model'

export const seedUsers = (amount: number): void => {
    console.info(`Feeding ${amount} users to db`)
    for (let i = 0; i < amount; i++) {
        fetch('https://randomuser.me/api/0.4/?randomapi')
        .then((res) => {
            return res.json()
        })
        .then((data) => {
            const {gender, name, location, email, picture, phone } = data.results[0].user
            const dbUser = {
                phone,
                email,
                gender,
                picture,
                location,
                title: name.title,
                lastName: name.last,
                firstName: name.first,
            }
            User.create(dbUser)
        })
        .catch((err) => {
            console.error(err)
        })
    }
}
