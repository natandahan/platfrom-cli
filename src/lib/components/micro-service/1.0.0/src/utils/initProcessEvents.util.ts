import { securityLogger } from '@app'

/**
 * @ProcessONEvents
 * 1. beforeExit
 * 2. disconnect
 * 3. exit
 * 4. rejectionHandled
 * 5. uncaughtException
 * 6. unhandledRejection
 * 7. warning
 * 8. message
 * 9. newListener
 * 10. removeListener
 * 11. multipleResolves
 */

const initProcessEventHandlers = (process: any) => {
    process.on('SIGINT', () => {
        securityLogger.error(`Gracefully shutting down micro front end`)
        process.exit()
    })
    process.on('unhandledRejection', (reason: any, promise: any) => {
        console.error('UNHANDLED REJECTION')
        console.error(reason)
        console.error(promise)
        securityLogger.error('Unhandled Rejection at:', promise, 'reason:', reason)
    })
    process.on('beforeExit', (code: any) => {
        securityLogger.error(`Process beforeExit event with code: ${code}`)
    })
    process.on('exit', (code: any) => {
        securityLogger.error(`Process exit event with code: ${code}`)
        process.exit()
    })
    process.on('uncaughtException', (error: any) => {
        console.error('UNCAUGHT EXCEPTIONS')
        console.error(error)
        securityLogger.error(error)
    })
}

export { initProcessEventHandlers }
