import {  CONFIG, aws } from '@app'

const { poolId, region } = CONFIG.OIDC
const {accessKeyId, secretAccessKey} = CONFIG.AWS
aws.config.update({accessKeyId, secretAccessKey})
const CognitoIDP = aws.CognitoIdentityServiceProvider
const client = new CognitoIDP({ apiVersion: '2016-04-19', region })

export interface IUser {
    MFAOptions?: any
    Username?: string
    Enabled?: boolean
    Attributes?: any[]
    UserStatus?: string
    UserCreateDate?: Date
    UserLastModifiedDate?: Date
}

export interface IResponse {
    Users?: IUser[]
    PaginationToken?: string
}

export interface IError {
    time: Date
    cfId: string
    code: string
    region: string
    message: string
    hostname: string
    requestId: string
    retryable: boolean
    statusCode: number
    retryDelay: number
    extendedRequestId: string
}

/**
 * @description list all users in current user pool
 */
export const listPollUsersDefaultOptions = {
    Limit: 10,
    UserPoolId: poolId,
    PaginationToken: null
} as aws.CognitoIdentityServiceProvider.ListUsersRequest

export const listPollUsers = (options = listPollUsersDefaultOptions): Promise<any> => {
    return new Promise((resolve, reject) => {
        client.listUsers({...options}, (err: IError, data: IResponse) => {
            if (err) {
                reject(err)
            }
            resolve(data.Users)
      })
    })
}

/**
 * @description get user info from poll
 */
export const getPollUser = (options: aws.CognitoIdentityServiceProvider.Types.AdminGetUserRequest): Promise<any> => {
    return new Promise((resolve, reject) => {
        client.adminGetUser({...options}, (err: IError, data: aws.CognitoIdentityServiceProvider.Types.AdminGetUserResponse) => {
            if (err) {
                reject(err)
            }
            resolve(data)
      })
    })
}
