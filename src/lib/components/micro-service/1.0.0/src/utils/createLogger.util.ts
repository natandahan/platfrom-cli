import fs from 'fs'
import CONFIG from '@env'
import * as winston from 'winston'

const { format, transports } = winston
const { level, timestamp } = CONFIG.LOGGER

interface ILogger extends winston.Logger {
    stream: any
}

/**
 * @createLogger
 */
const createLogger = (name: any, pathToLogFile: any): any => {
    const logger = winston.createLogger({
        level,
        format: format.combine(
            format.label({ label: name }),
            format.timestamp({ format: timestamp }),
            format.json()
        ),
        transports: [
            new transports.File({ filename : pathToLogFile }),
            new transports.Console({
                level,
                format: format.combine(
                    format.colorize(),
                    format.printf((info) => `[${info.level}] ${CONFIG.NAME} : ${info.timestamp} "${info.message}"`)
                )
            })
        ],
    }) as any
    logger.stream =  fs.createWriteStream(pathToLogFile, { flags: 'a' })
    return logger as ILogger
}

export { createLogger }
