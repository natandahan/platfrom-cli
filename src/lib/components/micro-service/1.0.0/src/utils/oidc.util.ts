import ENV from '@env'
import { jwt, fetch, jwkToPem } from '@app'

export enum OIDC_STATIC {
    RS256 = 'RS256',
    ACCESS = 'access',
    authCode = 'authorization_code',
    oAuthTokenPath = '/oauth2/token',
    oAuthCurrent = '/oauth2/userInfo',
    oAuthKeysPath = '.well-known/jwks.json',
    formUrlencoded = 'application/x-www-form-urlencoded'
}

/**
 * @getIdpCurrent
 * @description fetch current user
 * @returns {Object}
 */
export const getIdpCurrent = async (token: string) => {
    try {
        const oidcCurrentEndpoint = ENV.OIDC.endpoint + OIDC_STATIC.oAuthCurrent
        const basicAuth = 'Bearer ' + token
        const req = await fetch(oidcCurrentEndpoint, {
            headers: {
                Authorization: basicAuth,
            }
        })
        const data = await req.json()
        if (!data) {
            return undefined
        }
        return data
    } catch (err) {
        console.log(err)
        return undefined
    }
}

/**
 * @getIdpKeys
 * @description fetch identity provider public keys
 * @returns {Object}
 */
export const getIdpKeys = async () => {
    try {
        const { keys } = ENV.OIDC
        const req = await fetch(keys)
        const data = await req.json()
        if (!data) {
            return undefined
        }
        return data.keys
    } catch (err) {
        console.log(err)
        return undefined
    }
}

/**
 * @validateJwt
 * @description validate given jwt
 * 1. get identity provider public keys
 * 2. decode given token
 * 3. find token-header<>idp matching key
 * 4. convert jwt token to pem key
 * 5. verify token
 * 6. validate token issuer
 * 7. validate client id
 * 8. validate token use
 * @returns {Boolean}
 */
export const validateJwt = async (token: any) => {
    try {
        if (!token) { return undefined }
        const { clientId, issuer } = ENV.OIDC
        const idpKeys = await getIdpKeys()
        const decodedToken = jwt.decode(token, { complete: true }) as any
        const matchingKey = idpKeys.find((candidate: any) => candidate.kid === decodedToken.header.kid)
        const publicPem = jwkToPem(matchingKey)
        const claims = await jwt.verify(token, publicPem, { algorithms: [OIDC_STATIC.RS256] }) as any
        if (!claims) { return undefined }
        if (!claims.iss || claims.iss !== issuer) { return undefined }
        if (!claims.client_id || claims.client_id !== clientId) { return undefined }
        if (!claims.token_use || claims.token_use !== OIDC_STATIC.ACCESS) { return undefined }
        return true
    } catch (err) {
        // TODO SECURITY LOGGER EVENT: FAILED TO VALIDATE JWY, MORE-INFO:REASON
        console.log(err)
        return undefined
    }
}

export const exchangeCodeForJwt = async (code: string) => {
    try {
        const { endpoint, clientId, redirectUri, apiKey } = ENV.OIDC
        const oidcTokenEndpoint = endpoint + OIDC_STATIC.oAuthTokenPath
        const basicAuth = 'Basic ' + Buffer.from(clientId + ':' + apiKey).toString('base64')
        const urlEncoded = `code=${code}&client_id=${clientId}&redirect_uri=${redirectUri}&grant_type=${OIDC_STATIC.authCode}&`
        const req = await fetch(oidcTokenEndpoint, {
            method: 'POST',
            body: urlEncoded,
            headers: {
                'Authorization': basicAuth,
                'Content-Type': OIDC_STATIC.formUrlencoded
            }
        })
        return await req.json()
    } catch (err) {
        // TODO SECURITY LOG FAILED EXCHANGE
        console.log(err)
        return undefined
    }
}
