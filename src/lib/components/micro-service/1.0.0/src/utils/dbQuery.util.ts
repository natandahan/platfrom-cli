import { OPERATIONS } from '@db'

export interface IFilter {
    key: string
    value: string
    operation: string
}

export interface IFilterQuery {
    [key: string]: {
        [key: string]: string
    }
}

export const buildFilterQuery = (filterArr: IFilter[]): IFilterQuery[] => {
    const parsedArray = [] as IFilterQuery []
    filterArr.forEach((filter) => {
        const query = {
            [filter.key]: {
                [OPERATIONS[filter.operation]]: filter.value
            }
        }
        parsedArray.push(query)
    })
    return parsedArray
}
