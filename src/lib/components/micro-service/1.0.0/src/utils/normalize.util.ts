export const toCapital = (str: string): string => {
    const lowStr = str.toLowerCase()
    return lowStr.toLowerCase().charAt(0).toUpperCase() + lowStr.slice(1)
}
