import 'mocha'
import chai from 'chai'
import app from '../../app'
import chaiHttp from 'chai-http'
import HTTP from '../../lib/HttpCodes'

chai.use(chaiHttp)
const expect = chai.expect

describe('User controller API', () => {
    let testUserId = ''
    const testUser = {
        title: 'Architect',
        firstName: 'John',
        lastName: 'Doe',
        gender: 'male',
        phone: '6863407655',
        picture: './',
        username: 'john.doe',
        email: 'john.doe@some-mail.com',
        location: { city: 'Beverly Hills', state: 'CA'},
    }

    it('should create a test user', () => {
        return chai.request(app).post('/api/v1/users/').send(testUser).then((res) => { testUserId = res.body._id })
    })
    it('should return a newly created user and then delete it', () => {
        const expectedUser = {
            title: 'Chief',
            firstName: 'James',
            lastName: 'Munchkins',
            phone: '9293407655',
            username: 'james.chief',
            email: 'james.munchkins@some-mail.com',
            location: { city: 'New York', state: 'NY'},
        }
        return chai.request(app).post('/api/v1/users/').send(expectedUser).then((res) => {
            const actualUser = res.body
            chai.expect(res).to.have.status(HTTP.CREATED)
            chai.expect(actualUser.title).to.equal(expectedUser.title)
            chai.expect(actualUser.firstName).to.equal(expectedUser.firstName)
            chai.expect(actualUser.lastName).to.equal(expectedUser.lastName)
            chai.expect(actualUser.phone).to.equal(expectedUser.phone)
            chai.expect(actualUser.username).to.equal(expectedUser.username)
            chai.expect(actualUser.email).to.equal(expectedUser.email)
            chai.expect(actualUser.location.city).to.equal(expectedUser.location.city)
            chai.expect(actualUser.location.state).to.equal(expectedUser.location.state)
            /* REMOVE TEST USER */
            chai.request(app).delete(`/api/v1/users/${actualUser._id}`).then(() => { console.info(`[INFO] TEST USER HAS BEEN DELETED`) })
        })
    })
    it('should return a bad request error for create user, missing required properties', () => {
        const expectedUser = {
            title: 'Chief',
            firstName: 'Nick',
        }
        return chai.request(app).post('/api/v1/users/').send(expectedUser).then((res) => {
            chai.expect(res).to.have.status(HTTP.BAD_REQUEST)
        })
    })
    it(`should search and find ${testUser.username} user by id`, () => {
        return chai.request(app).get(`/api/v1/users/${testUserId}`).then((res) => {
            const searchUser = res.body
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(searchUser).to.be.an('object')
        })
    })

    it(`should list all users`, () => {
        return chai.request(app).get(`/api/v1/users`).then((res) => {
            const users = res.body
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(users).to.be.an('array')
        })
    })
    it('should replace user', () => {
        return chai.request(app).put(`/api/v1/users/${testUserId}`)
        .send(testUser).then((res) => {
            const ok = res.body.ok
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(ok).to.be.equal(1)
        })
    })

    it(`should update user title to 'professor'`, () => {
        return chai.request(app).patch(`/api/v1/users/${testUserId}`)
        .send({ title: 'professor'}).then((res) => {
            const searchUser = res.body
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(searchUser.title).to.be.equal('professor')
        })
    })

    it('should delete tester user', () => {
        return chai.request(app).delete(`/api/v1/users/${testUserId}`).then(() => { console.info(`[INFO] TEST USER HAS BEEN DELETED`) })
    })
})
