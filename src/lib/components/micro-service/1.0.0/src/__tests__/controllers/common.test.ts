import 'mocha'
import chai from 'chai'
import app from '../../app'
import chaiHttp from 'chai-http'
import HTTP from '../../lib/HttpCodes'

chai.use(chaiHttp)
const expect = chai.expect

describe('Common controller API', () => {
    let testUserId = ''
    const testUser = {
        title: 'Architect',
        firstName: 'John',
        lastName: 'Doe',
        gender: 'male',
        phone: '6863407655',
        picture: './',
        username: 'john.doe',
        email: 'john.doe@some-mail.com',
        location: { city: 'Beverly Hills', state: 'CA'},
    }

    it('should create a test user', () => {
        return chai.request(app).post('/api/v1/users/').send(testUser).then((res) => { testUserId = res.body._id })
    })
    it('should ping server and simply return "SUCCESS"', () => {
        return chai.request(app).get('/api/v1/ping/').then((res) => {
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(res.text).to.be.equal('SUCCESS')
        })
    })

    it('should return server health check', () => {
        return chai.request(app).get('/api/v1/health/').then((res) => {
            chai.expect(res).to.have.status(HTTP.OK)
            chai.expect(res.body.status).to.be.equal('Up')
        })
    })

    it('should return method not allowed', () => {
        return chai.request(app).post('/api/v1/health/').then((res) => {
            chai.expect(res).to.have.status(HTTP.METHOD_NOT_ALLOWED)
        })
    })

    it('should preform search query on User collection and return arr results', () => {
        return chai.request(app).get('/api/v1/search?q=user&limit=5&filter[0][key]=location.state&filter[0][operation]=equal&filter[0][value]=new jersey').then((res) => {
            console.log(res.body)
            chai.expect(res.body).to.be.an('object')
            chai.expect(res).to.have.status(HTTP.BAD_REQUEST)
        })
    })

    it('should delete tester user', () => {
        return chai.request(app).delete(`/api/v1/users/${testUserId}`).then(() => { console.info(`[INFO] TEST USER HAS BEEN DELETED`) })
    })
})
