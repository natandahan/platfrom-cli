import 'module-alias/register'
import * as validator from 'express-validator'
import DB from '@db'
import path from 'path'
import aws from 'aws-sdk'
import CONFIG from '@env'
import moment from 'moment'
import helmet from 'helmet'
import morgan from 'morgan'
import csv from 'csvtojson'
import express from 'express'
import routes from './routes'
import jwt from 'jsonwebtoken'
import scheduler from './jobs'
import fetch from 'node-fetch'
import jwkToPem from 'jwk-to-pem'
import CODES from '@lib/HttpCodes'
import bodyParser from 'body-parser'
import * as utils from '@utils/index'
import responseTime from 'response-time'
import cookieParser from 'cookie-parser'
import connectTimeout from 'connect-timeout'
import * as middleware from '@middlewares/index'

const app = express()
const accessLogger = utils.createLogger('access-logger', path.join(__dirname, '../logs/access.log'))
const securityLogger = utils.createLogger('security-logger', path.join(__dirname, '../logs/security.log'))

app.use(responseTime())
app.use(cookieParser())
app.use(bodyParser.json())
app.use(helmet(CONFIG.HELMET))
app.use(connectTimeout(CONFIG.TIMEOUT))
app.use(bodyParser.urlencoded({extended: true}))
app.use(morgan(CONFIG.LOGGER.format, {stream: accessLogger.stream}))

app.use('/', routes)
app.use(
    middleware.Http404,
    middleware.HttpException,
    middleware.MessageRespond,
    middleware.useTimeout,
    middleware.useValidator
)
utils.initProcessEventHandlers(process)

export {
    DB,
    aws,
    csv,
    jwt,
    fetch,
    CODES,
    CONFIG,
    moment,
    express,
    jwkToPem,
    scheduler,
    validator,
    accessLogger,
    securityLogger
}
export default app as any
