import mongoose from 'mongoose'
import ENV, { mongoUri } from '@env'
import { securityLogger } from '@app'
import { seedUsers } from '@utils/seedDb.util'

const { DB } = ENV

mongoose.Promise = Promise
mongoose.connect(mongoUri(), DB.options , (err) => {
    if (err) {
        console.log(`FAILED TO CONNECT DATABASE: ${err.message}`)
        throw new Error(err.message)
    } else {
        securityLogger.warn(`DB ${DB.name} is now connected`)
        // seedUsers(1000) // => CREATE RANDOM USERS
        // seedUsers(1000)
        // seedUsers(1000)
        // seedUsers(1000)
    }
})

export interface IOperations {
    [key: string]: any
}

export const OPERATIONS = {
    equal: '$eq',
    greater: '$gt',
    include : '$in',
    less_than : '$lt',
    not_equal : '$ne',
    not_include : '$nin',
    greater_or_equal : '$gte',
    less_than_or_equal : '$lte',
} as IOperations

export { mongoose }
export default mongoose.connection
