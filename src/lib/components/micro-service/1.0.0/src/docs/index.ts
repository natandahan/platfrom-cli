import user from './user.doc'
import common from './common.doc'
import swaggerJsdoc from 'swagger-jsdoc'

export const routes = {
    definitions: {
        User: user.definition,
        id: common.id.definition,
    },
    paths: {...user.paths, ...common​​.paths},
    tags: [ user.tag, common.tag ],
}

export const swaggerDefinition = {
    ...routes,
    basePath: '/',
    openapi: '3.0.0',
    produces: [ 'application/json'],
    consumes: [ 'application/json' ],
    servers: [
        {
            description: 'Localhost',
            url: '/api/v1/',
        },
    ],
    info: {
        description: 'Simple TS Express server',
        license: {
            name: 'MIT',
            url: 'https://opensource.org/licenses/MIT'
        },
        title: 'Common Express Api',
        version: '1.0.0',
    },
}

export const jsDocOptions = {
    apis: ['../routes'],
    swaggerDefinition,
}

export const options = { explorer: true }
export const specs = swaggerJsdoc(jsDocOptions)
