const common = {
    id: {
        definition: {
            properties: {
                uuid: {
                    type: 'string'
                }
            }
        }
    },
    tag: {
        description: 'API Common endpoints',
        name: 'Common',
    },
    paths: {
        '/oidc': {
            get: {
              tags: ['Common'],
              summary: 'OIDC-Flow Token exchange handler path',
              parameters: [
                {
                  in: 'query',
                  name: 'code',
                  required: true,
                  description: 'TOKEN',
                  schema: {
                    type: 'string'
                  }
                }
              ],
              responses: {
                200: {
                  description: 'OK',
                  schema: {
                    type: 'string'
                  }
                }
              }
            },
        },
        '/ping': {
            get: {
              tags: ['Common'],
              summary: 'Respond to PING request',
              responses: {
                200: {
                  description: 'OK',
                  schema: {
                    type: 'string'
                  }
                }
              }
            },
        },
        '/health': {
            get: {
              tags: ['Common'],
              summary: 'Returns current service health status',
              responses: {
                200: {
                  description: 'OK',
                  schema: {
                    type: 'string'
                  }
                }
              }
            },
        },
        '/info': {
            get: {
              tags: ['Common'],
              summary: 'Service basic information',
              responses: {
                200: {
                  description: 'OK',
                  schema: {
                    type: 'string'
                  }
                }
              }
            },
        },
        '/jobs': {
            get: {
              tags: ['Common'],
              summary: 'Service active jobs',
              responses: {
                200: {
                  description: 'OK',
                  schema: {
                    type: 'object'
                  }
                }
              }
            },
        },
    }
}

export default common
