import { userSchema } from '@models/user.model'

const parseDBSchema = (schema: any) => {
  const data = userSchema as any
  const parsedProperties = {} as any
  const pathsNames = Object.keys(data.paths)
  pathsNames.forEach((p: string) => {
    parsedProperties[`${p}`] = {type: data.paths[p].instance.toLowerCase()}
  })
  // REMOVE DB CREATED PROPERTIES
  delete parsedProperties.__v
  delete parsedProperties._id
  delete parsedProperties.updatedAt
  delete parsedProperties.createdAt
  return { ...parsedProperties }
}

const user = {
  tag: {
    description: 'API for Users in the system',
    name: 'Users',
  },
  definition: {
    type: 'object',
    properties: parseDBSchema(userSchema),
  },
  paths: {
    '/users': {
      get: {
        tags: ['Users'],
        summary: 'Get all users in system',
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          }
        }
      },
      post: {
        tags: [
          'Users'
        ],
        summary: 'Create a new user in system',
        requestBody: {
          description: 'User Object',
          required: true,
          content: {
            'application/json': {
              schema: {
                $ref: '#/definitions/User'
              }
            }
          }
        },
        produces: [
          'application/json'
        ],
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/id'
            }
          },
          400: {
            description: 'Failed. Bad post data.'
          }
        }
      }
    },
    '/users/exist': {
      get: {
        tags: ['Users'],
        summary: 'Check if user for active session exists in DB',
        responses: {
          200: {
            description: 'OK',
            schema: {
              type: 'boolean',
            }
          },
          500: {
            description: 'INTERNAL SERVE ERROR',
          }
        }
      },
    },
    '/users/current': {
      get: {
        tags: ['Users'],
        summary: 'Return user object for active session exists in DB',
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          }
        }
      },
    },
    '/users/schema': {
      get: {
        tags: ['Users'],
        summary: 'Return user schema',
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          }
        }
      },
    },
    '/users/{id}': {
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'ID of the user that we want to match',
          type: 'string'
        }
      ],
      get: {
        tags: [
          'Users'
        ],
        summary: 'Get user with given ID',
        parameters: [
          {
            in: 'path',
            name: 'id',
            required: true,
            description: 'User with id',
            schema: {
              $ref: '#/definitions/id'
            }
          }
        ],
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          },
          404: {
            description: 'Failed. User not found.'
          }
        }
      },
      put: {
        summary: 'Replace user object with given ID',
        tags: [
          'Users'
        ],
        requestBody: {
          description: 'User Object',
          required: true,
          content: {
            'application/json': {
              schema: {
                $ref: '#/definitions/User'
              }
            }
          }
        },
        parameters: [
          {
            in: 'path',
            name: 'id',
            required: true,
            description: 'User with new values of properties',
            schema: {
              $ref: '#/definitions/id'
            }
          }
        ],
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          },
          400: {
            description: 'Failed. Bad post data.'
          },
          404: {
            description: 'Failed. User not found.'
          }
        }
      },
      patch: {
        summary: 'Update user object with given ID',
        tags: [
          'Users'
        ],
        requestBody: {
          description: 'User Object',
          required: true,
          content: {
            'application/json': {
              schema: {
                $ref: '#/definitions/User'
              }
            }
          }
        },
        parameters: [
          {
            in: 'path',
            name: 'id',
            required: true,
            description: 'User with new values of properties',
            schema: {
              $ref: '#/definitions/id'
            }
          }
        ],
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/User'
            }
          },
          400: {
            description: 'Failed. Bad post data.'
          },
          404: {
            description: 'Failed. User not found.'
          }
        }
      },
      delete: {
        summary: 'Delete user with given ID',
        tags: [
          'Users'
        ],
        parameters: [
          {
            in: 'path',
            name: 'id',
            required: true,
            description: 'Delete User with id',
            schema: {
              $ref: '#/definitions/id'
            }
          }
        ],
        responses: {
          200: {
            description: 'OK',
            schema: {
              $ref: '#/definitions/id'
            }
          },
          404: {
            description: 'Failed. User not found.'
          }
        }
      }
    }
  }
}

export default user
