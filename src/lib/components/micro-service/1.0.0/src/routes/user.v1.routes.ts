import { express } from '@app'
import { useAuth } from '@middlewares/index'
import * as Validator from '@validator/index'
import * as Controller from '@controllers/index'

const router = express.Router()

router.route('/')
.get(Validator.get, useAuth, Controller.listAll)
.post(Validator.validateUser, Controller.createUser)
.all(Controller.notAllowed)

router.route('/exist')
.get(Validator.get, useAuth, Controller.isUserExist)
.all(Controller.notAllowed)

router.route('/current')
.get(Validator.get, useAuth, Controller.getCurrentUser)
.all(Controller.notAllowed)

router.route('/schema')
.get(Validator.get, useAuth, Controller.getUserSchema)
.all(Controller.notAllowed)

router.route('/autocomplete')
.get(Validator.get, useAuth, Controller.autocompleteByName)
.all(Controller.notAllowed)

router.route('/:userId')
.get(Validator.userID , Controller.findUserById)
.delete(Validator.userID, Controller.deleteUser)
.put(Validator.userSchema, Controller.replaceUser)
.patch(Validator.userSchema, Controller.updateUser)
.all(Controller.notAllowed)

export default router
