import { express } from '@app'
import { specs, options } from '@docs/index'
import { get, search } from '@validator/index'
import { useAuth } from '@middlewares/index'
import { healthCheck, pingCheck, schedulerJobs } from '@controllers/index'
import { serve, setup } from 'swagger-ui-express'
import { notAllowed, searchQuery, oidcTokenHandler, oidcLogout, oidcSigning, serverInfo } from '@controllers/index'

const router = express.Router()

router.route('/oidc')
.get(get, oidcTokenHandler)
.all(notAllowed)

router.route('/ping')
.get(get, pingCheck)
.all(notAllowed)

router.route('/health')
.get(get, healthCheck)
.all(notAllowed)

router.route('/info')
.get(get, serverInfo)
.all(notAllowed)

router.route('/jobs')
.get(get, useAuth, schedulerJobs)
.all(notAllowed)

router.route('/search') // TODO ADD SUBJECT QUERY AND REDIRECT FROM SUBJECT ROUTES
.get(search, useAuth, searchQuery)
.all(notAllowed)

router.route('/auth-flow') // TODO -> SWAGGER DOCUMENTATION
.get(get, useAuth)
.all(notAllowed)

router.use('/documentation', serve) // TODO -> SWAGGER DOCUMENTATION
.route('/documentation')
.get(useAuth, setup(specs, options))
.all(notAllowed)

router.route('/signin') // TODO -> SWAGGER DOCUMENTATION
.get(get, useAuth, oidcSigning)
.all(notAllowed)

router.route('/signout') // TODO -> SWAGGER DOCUMENTATION
.get(get, oidcLogout)
.all(notAllowed)

export default router
