import { express } from '@app'
import userRouter from './user.v1.routes'
import commonRouter from './common.v1.routes'

const router = express.Router()

router.use('/api/v1/users', userRouter)
router.use('/api/v1', commonRouter)

export default router
