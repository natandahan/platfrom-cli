import { IRequest } from '@app-types'
import { CODES, CONFIG, express } from '@app'
import User, { userSchema } from '@models/user.model'

/**********
 * @User_Actions
 *********/
export const USER_ACTIONS = [
   'autocompleteByName',
   'createUser',
   'deleteUser',
   'findUserById',
   'getCurrentUser',
   'getUserSchema',
   'isUserExist',
   'listAll',
   'replaceUser',
   'updateUser'
]

/**
 * @method autocompleteByName
 * @summary Autocomplete mapping for user firs and last name
 * @returns {Array} On success responds with <200|Ok> []{ id, label }
 */
export const autocompleteByName = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const { q } = req.query
      const _ATTR = [
         {lastName: {$regex: q, $options: 'i'}},
         {firstName: {$regex: q, $options: 'i'}}
      ]
      const _$ = { $or: _ATTR }
      const _OPTIONS = { _id: 1, firstName: 1, lastName: 1}

      const query = (await User.find(_$, _OPTIONS).limit(CONFIG.DB.defaults.limit))
      const docs = await query

      if (!docs || docs.length === 0) {
         return res.status(CODES.NOT_FOUND).end()
      } else {
         return res.status(CODES.OK).json(docs).end()
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method creatUser
 * @summary Create new user
 * @body {user} User [firstName] [lastName] [username] [gender] [location] [email] [phone] [title]
 * @returns {Object} On success responds with <201|Created> with newly created user
 */
export const createUser = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const user = await User.create(req.body)
      res.status(CODES.CREATED).json(user)
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method deleteUser
 * @summary Delete user
 * @param {userId} ID uuid pass as params <required>
 * @returns {Object} On success responds with <200|Ok> with removed user
 */
export const deleteUser = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const userId  = req.params.userId ? req.params.userId : req.body.userId as string
      const removedUser = await User.findOneAndRemove({_id: userId})
      res.status(CODES.OK).json(removedUser)
   } catch (err) {
      if (err.status || err.message) {
         nxt(err)
      }
      nxt({ message: `Failed to update user`})
   }
}

/**
 * @method findUserById
 * @summary Find user by id
 * @param {userId} ID uuid pass as params <required>
 * @returns {Object} On success responds with <200|Ok> with user object
 */
export const findUserById = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const userId  = req.params.userId ? req.params.userId : req.body.userId as string
      // const foundUser = await UP.getPollUser({Username: userId, UserPoolId: CONFIG.OIDC.poolId})
      const foundUser = await User.findById({ _id: userId})
      if (foundUser) {
         res.status(CODES.OK).json(foundUser)
      } else {
         nxt({ status: CODES.NOT_FOUND, message: 'No user found'})
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method getCurrentUser
 * @summary Get current active user
 * @returns {Object} On success responds with <200|Ok> user
 */
export const getCurrentUser = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      if (!req.user) { return res.status(CODES.INTERNAL_SERVER_ERROR).end() } // TODO ERROR SECURE PATH WITHOUT A USER!
      res.status(CODES.CREATED).json(req.user)
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method getUserSchema
 * @summary Returns user schema as defined in service
 * @returns {Object} On success responds with <200|Ok> Schema
 */
export const getUserSchema = (_: IRequest, res: express.Response): any => {
   return res.status(CODES.OK).json(userSchema.obj).end()
}

/**
 * @method isUserExist
 * @summary Check if current user exist in db
 * @returns {Object} On success responds with <200|Ok> true | false
 */
export const isUserExist = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      if (!req.user) { return res.status(CODES.INTERNAL_SERVER_ERROR).end() } // TODO -> INTERNAL ERROR, NO USER IN A !! SECURE PATH !!
      const registerWithEmail = req.user.email ? ((await User.find({ email: req.user.email})).length > 0) : undefined
      const registerWithPhone = req.user.phone ? ((await User.find({ phone: req.user.phone})).length > 0) : undefined
      if (registerWithEmail || registerWithPhone) {
         return res.status(CODES.OK).send(true).end()
      } else {
         return res.status(CODES.OK).send(false).end()
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method listAll
 * @summary List all users in db <within default limits>
 * @returns {Array} On success responds with <200|Ok> with users array
 */
export const listAll = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      // const users = await UP.listPollUsers()
      const users = await User.find().limit(CONFIG.DB.defaults.limit)
      res.status(CODES.OK).json(users)
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method replaceUser
 * @summary Replace existing user
 * @param {userId} ID uuid pass as params <required>
 * @body {user} User [firstName] [lastName] [username] [gender] [location] [email] [phone] [title]
 * @returns {Object} On success responds with <200|Ok> with newly replaced user
 */
export const replaceUser = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const userId  = req.params.userId ? req.params.userId : req.body.userId as string
      const newlyReplacedUser = await User.collection.findOneAndReplace({_id: userId}, req.body)
      res.status(CODES.OK).json(newlyReplacedUser)
   } catch (err) {
      if (err.status || err.message) {
         nxt(err)
      }
      nxt({ message: `Failed to replace user`})
   }
}

/**
 * @method updateUser
 * @summary Update user
 * @param {userId} ID uuid pass as params <required>
 * @body {user} User [firstName] [lastName] [username] [gender] [location] [email] [phone] [title]
 * @returns {Object} On success responds with <200|Ok> with update user
 */
export const updateUser = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      const userId  = req.params.userId ? req.params.userId : req.body.userId as string
      const newlyUpdateUser = await User.findOneAndUpdate({_id: userId}, req.body, { new: true })
      res.status(CODES.OK).json(newlyUpdateUser)
   } catch (err) {
      if (err.status || err.message) {
         nxt(err)
      }
      nxt({ message: `Failed to update user`})
   }
}
