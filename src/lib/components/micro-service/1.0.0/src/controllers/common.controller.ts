
import { IRequest } from '@app-types'
import listEndpoints from 'express-list-endpoints'
import app, { CODES, CONFIG, DB, express, scheduler, accessLogger } from '@app'
import { buildFilterQuery, toCapital, exchangeCodeForJwt, getIdpCurrent } from '@utils/index'
import { userSchema } from '@models/user.model'
import { User } from 'aws-sdk/clients/appstream'

/**********
 * @Common_Actions
 *********/
export const COMMON_ACTIONS = [
   'healthCheck',
   'notAllowed',
   'oidcCurrent',
   'oidcLogout',
   'oidcSigning',
   'oidcTokenHandler',
   'pingCheck',
   'schedulerJobs',
   'searchQuery',
   'serverInfo'
]
/**
 * @method healthCheck
 * @summary Check server health and respond with status and message
 * @returns {Object} Health check results
 * @example https://cloud.ibm.com/docs/node?topic=nodejs-node-healthcheck
 */
export const healthCheck = async ({}, res: express.Response): Promise<any> => {
   // TODO IMPLEMENT APP STATUS [Starting 503, Up 200, Stopping 503, Down 503, Errored 500]
   res.status(CODES.OK).json({status: 'Up'})
}

/**
 * @method notAllowed
 * @summary Method not allowed controller
 * @returns {Object} Message "Method not allowed"
 */
export const notAllowed = (req: express.Request, res: express.Response): any => {
   // TODO ADD SECURITY LOGGER
   console.warn(`[WARN] Method not allowed ::${req.headers.host}`)
   res.status(CODES.METHOD_NOT_ALLOWED).json({message: 'Method not allowed'})
}

/**
 * @method oidcCurrent
 * @summary Open Id Connect Current User
 * @returns {Object} current user
 */
export const oidcCurrent = async (req: IRequest, res: express.Response, nxt: any): Promise<any> => {
   try {
      if (!req.user) {
         nxt(Error('User not found'))
      } else {
         res.status(CODES.OK).json(req.user)
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method oidcLogout
 * @summary Open Id Connect Logout
 * @returns {Object} current user
 */
export const oidcLogout = async (req: express.Request, res: express.Response, nxt: any): Promise<any> => {
   try {
      const { cookieId, endpoint, clientId } = CONFIG.OIDC
      const uri = req.protocol + '://' + req.get('host') + '/api/v1/signout'
      if (req.cookies && req.cookies[cookieId]) {
         res.clearCookie(cookieId)
         const redirectUrl = `${endpoint}/logout?client_id=${clientId}&logout_uri=${uri}`
         return res.redirect(redirectUrl)
      }
      nxt({ message: { title: 'SUCCESSFULLY LOGGED OUT' } })
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method oidcSigning
 * @summary Open Id Connect Signing [PRIVATE ROUTE]
 * @returns {Message} 200
 */
export const oidcSigning = async (req: express.Request, res: express.Response, nxt: any): Promise<any> => {
   try {
      nxt({ message: { title: 'SIGNED IN' } })
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method oidcTokenHandler
 * @summary Open Id Connect Handler
 * @query {code} - one time use code
 * @returns set cookie in browser
 */
export const oidcTokenHandler = async (req: express.Request, res: express.Response, nxt: any): Promise<any> => {
   try {
      const oneTimeCode = req.query.code ? req.query.code : undefined
      if (!oneTimeCode) {
         return nxt({err: {message: 'INVALID CODE'}})
      }
      const jwt = await exchangeCodeForJwt(oneTimeCode)
      if (jwt && jwt.access_token) {
         res.cookie(CONFIG.OIDC.cookieId, jwt.access_token, {
            httpOnly: true,
         })
         res.redirect('/api/v1/auth-flow')
      }
      if (jwt && jwt.error) {
         res.status(CODES.BAD_REQUEST).send(jwt.error).end()
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method pingCheck
 * @summary Check server response
 * @returns {String} SUCCESS
 */
export const pingCheck = ({}, res: express.Response): void => {
   res.status(CODES.OK).send('SUCCESS').end()
}

/**
 * @method schedulerJobs
 * @summary Returns all schedule jobs in server queue
 * @returns {Array} of jobs
 */
export const schedulerJobs = async ({}, res: express.Response, nxt: any): Promise<any> => {
   try {
      const jobs = await scheduler.jobs({})
      res.json(jobs)
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method searchQuery
 * @summary Search query controller, preform database query base on request query
 * @query {Url} [q,sort,filter,limit,offset,filter]
 * @returns {Any} Query results
 */
export const searchQuery = async (req: express.Request, res: express.Response, nxt: express.NextFunction): Promise<any> => {
   try {
      const sort = req.query.sort || null as string
      const offset = req.query.offset || 0 as number
      const search = toCapital(req.query.q) || '' as string
      const limit = req.query.limit || CONFIG.DB.defaults.limit as number
      const filter = req.query.filter ? buildFilterQuery(req.query.filter) : null as any[]

      delete req.query.q
      delete req.query.sort
      delete req.query.offset
      delete req.query.limit
      delete req.query.filter

      const findOptions = filter ? Object.assign({}, req.query, ...filter) : req.query

      if (DB.modelNames().includes(search)) {
         const query = DB.models[search].find(findOptions).skip(offset).sort(sort).limit(limit)
         const docs = await query
         res.status(CODES.OK).json(docs)
      } else {
         nxt({ status: CODES.BAD_REQUEST, message: `Invalid search term '${search}'`})
      }
   } catch (err) {
      nxt(err)
   }
}

/**
 * @method serverInfo
 * @summary Get server info
 * @returns {ServerPublicInfo} 200
 */
export const serverInfo = async (req: express.Request, res: express.Response, nxt: any): Promise<any> => {
   try {
      const routes = listEndpoints(app)
      const manifest = {
         'Name': CONFIG.NAME,
         'Auth Enabled': (CONFIG.AUTH === true), // RETURN BOOL ! STRING
         'Description': CONFIG.DESCRIPTION,
         'Features': JSON.parse(CONFIG.FEATURES),
         'Routes': routes
      }
      return res.status(CODES.OK).json(manifest)
   } catch (err) {
      nxt(err)
   }
}
