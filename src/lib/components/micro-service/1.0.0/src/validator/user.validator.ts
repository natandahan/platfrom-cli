import { validator } from '@app'

const { body, param } = validator

// TODO ADD COSTUME MESSAGES
export const validateUser = [
    body('username').optional().bail().isString(),
    body('firstName').optional().bail().isString(),
    body('lastName').optional().bail().isString(),
    body('email').optional().bail().isEmail(),
    body('gender').optional().isString(),
    body('location').optional(),
    body('phone').optional().isMobilePhone('any'),
    body('picture').optional().isString(),
    body('title').optional().bail().isString(),
] as any[]

export const userSchema = [
    body('username').isString(),
    body('firstName').isString(),
    body('lastName').isString(),
    body('email').isEmail(),
    body('gender').isString(),
    body('location'),
    body('phone').isMobilePhone('any'),
    body('picture').isString(),
    body('title').isString(),
]

export const userID = [
    param('userId').exists().isString()
]
