import { validator } from '@app'
const { query, body } = validator

// TODO IMPLEMENT COSTUME ERRORS FOR SECURITY/DOCS
export const get = [
    body('*').isEmpty(),
    query('*').isEmpty(),
] as any[]

export const search = [
    query('sort').optional().isString(),
    query('limit').optional().isNumeric().toInt(),
    query('offset').optional().isNumeric().toInt(),
]
