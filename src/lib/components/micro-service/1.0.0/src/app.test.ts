import chai from 'chai'
import chaiHttp from 'chai-http'
import 'mocha'
import HTTP from './lib/HttpCodes'
import app from './app'

chai.use(chaiHttp)
const expect = chai.expect

describe('Hello API Request', () => {
  it('should return response on call', () => {
    return chai.request(app).get('/')
      .then((res) => {
        expect(res).to.have.status(HTTP.NOT_FOUND)
      })
  })

  it('should return users array', () => {
    return chai.request(app).get('/api/v1/users/')
      .then((res) => {
        chai.expect(res).to.have.status(200)
        chai.expect(res.body).to.be.a('array')
      })
  })
})
