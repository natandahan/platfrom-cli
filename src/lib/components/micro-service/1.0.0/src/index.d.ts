import * as app from '@app'
import { Document } from 'mongoose'

// # USER INTERFACE
export interface IUser extends Document {
    name: string
    email: string
    address?: any
    picture?: any
    phone?: string
    title?: string
    gender?: string
    lastName?: string
    username?: string
    firstName?: string
}

// # JWT USER INTERFACE
export interface IJWTUser extends IUser {
    sub: string
    given_name?: string
    family_name?: string
    email_verified: boolean
}

// #IRequest APP REQUEST INTERFACE 
export interface IRequest extends app.express.Request {
    user?: IJWTUser
}
