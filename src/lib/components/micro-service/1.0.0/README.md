![](https://15f76u3xxy662wdat72j3l53-wpengine.netdna-ssl.com/wp-content/uploads/2018/02/Google-Pub-Sub-connector-150x150.png)
# Documentation
-----
MICRO_SERVICE_NAME Micro Service
> @Natan Dahan <contact@natandahan.com> | Palo Alto CA

## **Installation**
Install packages and create dist
```bash
$npm install && npm run build
```

## **Quickstart**
Npm for a quick start, npm run dev for hot realoading
```bash
$npm start
$npm run dev
```

## **Tests**
Run test cases and create cover report
```bash
$npm test
$npm run coverage
```

## **Linting**
For lint syntax, and code styling run
```bash
$npm run lint
```

## **More Info**
Additional information can be found at the following links
- [Project Homepage](https://bitbucket.org/natandahan/MICRO_SERVICE_NAME/src/develop/)
- [TSLint code styling rules](https://palantir.github.io/tslint/rules/)
- [Mongodb client lib](https://mongoosejs.com/docs/guide.html)
- [Mocha testing](https://mochajs.org)